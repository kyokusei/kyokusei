// ========== 極性 -Kyokusei- ============================ game_states.h ======================= //
#ifndef LOGIC_GAME_STATE
#define LOGIC_GAME_STATE

// TODO: add game states here.
typedef enum GameState
{
    Title,
    InGame,
    Paused, // idk if i'll need this or not
    InMenu,
    Credits
} GameState;

typedef enum InGameState
{
    Platforming,
    Overworld
} InGameState;

typedef enum MenuState
{
    TitleMenu,
    PauseMenu,
    ShopMenu,
    EquipMenu,
    InventoryMenu,
    MapMenu,
    OptionsMenu
} MenuState;

#endif