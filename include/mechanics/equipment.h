// ========== 極性 -Kyokusei- ============================ equipment.h ========================= //
#ifndef MECH_EQUIPMENT
#define MECH_EQUIPMENT

#include "actors/player.h"
#include "items.h"
// TODO: implement equipment items and the like. gonna have to see about simplifying things.

typedef enum EquipType
{
    Blade,
    Gun,
    Accessory,
    Cloak
} EquipType;

typedef struct Equipment
{
    Item item_info;
    EquipType type;

} Equipment;

void UpdateStats(Player *player, Equipment *gear);

#endif