// ========== 極性 -Kyokusei- ============================ elements.h ========================== //
#ifndef MECH_ELEMENTS
#define MECH_ELEMENTS

// TODO: implement the elements as enums, most likely, as well as some other functions, like 
// weaknesses and resistances.

typedef enum Element
{
    Force,
    Blaze,
    Aqua,
    Lightning,
    Zephyr,
    Nuke,
    Psi,
    Holy,
    Ruin
} Element;

#endif