// ========== 極性 -Kyokusei- ============================ items.h ============================= //
#ifndef MECH_ITEMS
#define MECH_ITEMS

// TODO: implement items and their functions

typedef enum ItemType
{
    Consumable,
    Equipable,
    KeyItem
} ItemType;

typedef struct Item
{
    u8 item_id;
    ItemType type;
    u32 value;
    u32 sell_value;
} Item;

#endif