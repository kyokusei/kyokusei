// ========== 極性 -Kyokusei- ============================ ailments.h ========================== //
#ifndef MECH_STATUS
#define MECH_STATUS

typedef enum StatusAilment
{
    Fine,
    Poison,
    Burn,
    Frost,
    Shock,
    Slow,
    Curse,
    Blind,
    Bleed
} StatusAilment;

typedef enum StatusDeath
{
    Alive,
    Deceased
} statusDeath;

// typedef enum 

typedef enum Polarity
{
    Keen,
    Neutral,
    Impulse    
} Polarity;

#endif