// ========== 極性 -Kyokusei- ============================ inventory.h ========================= //
#ifndef MECH_INVENTORY
#define MECH_INVENTORY

#include "items.h"
// TODO: implement inventory functions and the array for the inventory itself, so that the player
// can keep items and use them.
typedef struct Inventory
{
    Item item_info;
    u8 quantity;
} Inventory;



#endif