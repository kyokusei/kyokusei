// ========== 極性 -Kyokusei- ============================ enemy.h ============================= //
#ifndef ACTOR_ENEMY
#define ACTOR_ENEMY

#include <tonc_types.h>

#include "../status.h"
#include "actor_base.h"

typedef struct enemy
{
    ActorData a_info;
    u8 enemy_id;
    char *name;
    u16 level;
    u16 exp;
    Stats stats;
    Resistances resists;
    StatusAilment ailment;
    bool is_dead;
} enemy;

// // void setHP(u16 hp);
// s8 get_health();
// s8 get_mana();
// StatusAilment get_status();

#endif