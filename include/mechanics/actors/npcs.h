// ========== 極性 -Kyokusei- ============================ npcs.h ============================== //
#ifndef ACTOR_NPC
#define ACTOR_NPC

#include <tonc_types.h>
#include <tonc_math.h>

typedef enum NPCType
{
    Story, Shopkeep, Generic, 
} NPCType;

typedef struct NPC
{
    ActorData a_info;
    NPCType npc_type;
} NPC;

#endif