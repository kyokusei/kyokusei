// ========== 極性 -Kyokusei- ============================ Player.hpp ========================== //
#include <tonc_types.h>
#include "actor_base.hpp"

class Player
{
    u32 max_hp;
    u32 max_mp;
    s32 cur_hp;
    s32 cur_mp;
    u32 level;
    u32 exp;
    u32 next_lvl;
private:
    /* data */
public:
    Player(/* args */);
    ~Player();
    u32 getHP();
    u32 getMP();
    u32 getEXP();
    u32 is_alive();
};

Player::Player(/* args */)
{
}

Player::~Player()
{
}

u32 Player::getHP()
{ return cur_hp; }

u32 Player::getMP()
{ return cur_mp; }

// Player::LoadFromSave()
// {

// }
