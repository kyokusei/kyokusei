// ========== 極性 -Kyokusei- ============================ player.h ============================ //
#ifdef __cplusplus
extern "C" {
#endif

#ifndef ACTOR_PLAYER
#define ACTOR_PLAYER

#include <tonc_types.h>
// #include <ctonc_types>

#include "../status.h"
#include "actor_base.h"

typedef struct Player
{
    ActorData a_info;
    u32 level;
    u32 exp;
    u32 next_lvl;
    Polarity forme;
    u32 currency;
    Stats stats;
    Resistances resists;
    StatusAilment ailment;
    bool is_dead;
} Player;


#endif

#ifdef __cplusplus
};
#endif