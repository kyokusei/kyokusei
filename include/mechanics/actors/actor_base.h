// ========== 極性 -Kyokusei- ============================ actor_base.h ======================== //
#ifndef MECH_ACTOR_BASE
#define MECH_ACTOR_BASE

#include <tonc_types.h>

#include "../../rendering/sprites.h"


typedef enum ActorType
{
    NPCActor,
    PlayerActor,
    EnemyActor,
    InteractableActor,
    ProjectileActor,
    SlashActor,
    MagicActor
} ActorType;

typedef struct ActorData
{
    u32         actor_id;
    char        *name;
    ActorType   actor_type;
    Sprite      sprite;
} ActorData;

typedef struct Stats
{
    u8 max_hp,   max_mp;
    s8 cur_hp,   cur_mp;
    u8 phys_atk, phys_def;
    u8 arc_ark,  arc_def;
    u8 strength, dexterity;
    u8 arcana,   vitality;
} Stats;

typedef struct Resistances
{
    u8 res_blaze:4;
    u8 res_aqua:4;
    u8 res_elec:4;
    u8 res_wind:4;
    u8 res_nuke:4;
    u8 res_psi:4;
    u8 res_hoiy:4;
    u8 res_ruin:4;
} ALIGN4 Resistances;

#endif