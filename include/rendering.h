// ========== 極性 -Kyokusei- ============================ rendering.h ========================= //
#ifndef RENDERING
#define RENDERING

#include "rendering/map_info.h"
#include "rendering/sprites.h"
#include "rendering/textbox.h"
#include "rendering/viewport.h"

Viewport g_viewport =
{
    0, 0, 1024, 240,
    0, 0, 1024, 160,
};

MapInfo g_background;

// OBJ_ATTR obj_buffer[128];

Sprite g_slime;


#endif