// ========== 極性 -Kyokusei- ============================ mechanics.h ========================= //
#ifndef MECHANICS
#define MECHANICS

#include "mechanics/actors.h"
#include "mechanics/dialogue.h"
#include "mechanics/elements.h"
#include "mechanics/equipment.h"
#include "mechanics/inventory.h"
#include "mechanics/items.h"
#include "mechanics/status.h"

#endif