// ========== 極性 -Kyokusei- ============================ map_info.h ========================== //
#ifndef __RENDER_MAP_INFO__
#define __RENDER_MAP_INFO__

#include <tonc.h>
#include <tonc_types.h>

#include "viewport.h"

#define kakarikoPitch   128 // this is here for the kakariko vilage bg for now. (for testing)

typedef struct MapInfo
{
    union
    {
        u32 bgState;  // background state
        struct
        {
            u16 flags;
            u16 count;
        };
    };
    SCR_ENTRY *dstMap; // destination data: screenblock pointer
    SCR_ENTRY *srcMap; // source data: source map address
    u32 srcMapWidth;   // source data: width
    u32 srcMapHeight;  // source data: height
    FIXED mapX;        // source data: fixed decimal x-coord on map (.8f)
    FIXED mapY;        // source data: fixed decimal y-coord on map (.8f)
} MapInfo;


// -- prototypes --
void bg_tile_init(MapInfo *bg_tile, int bgnr, u32 ctrl, const void *map, u32 map_width, u32 map_height);
void bg_tile_column_copy(MapInfo *bg_tile, int tx, int ty);
void bg_tile_row_copy(MapInfo *bg_tile, int tx, int ty);
void bg_tile_update(MapInfo *bg_tile, Viewport *view);

#endif