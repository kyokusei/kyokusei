// ========== 極性 -Kyokusei- ============================ gfx_master.h ======================== //
#ifndef __RENDER_SPRITE_GFX_MASTER__
#define __RENDER_SPRITE_GFX_MASTER__

// TODO: add both the headers within the gfx folder, and also the files in rendering/sprites

#include "../../gfx/echo.h"
#include "../../gfx/emy_chroncha.h"
#include "../../gfx/emy_nanite_wave.h"
#include "../../gfx/emy_sentry.h"
#include "../../gfx/emy_slime.h"
#include "../../gfx/emy_trent.h"

#include "sprites/enemy_chroncha.h"
#include "sprites/enemy_nanite_wave.h"
#include "sprites/enemy_sentry.h"
#include "sprites/enemy_slime.h"
#include "sprites/enemy_trent.h"
#include "sprites/player_echo.h"

#endif