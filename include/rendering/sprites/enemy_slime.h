// ========== 極性 -Kyokusei- ============================ enemy_slime.h ========================== //
#ifndef __SPRITE_ENEMY_SLIME__
#define __SPRITE_ENEMY_SLIME__

#include <tonc.h>

#include "../sprites.h"
#include "../viewport.h"

// #include "rendering/sprites/enemy_slime.h"

#define SLIME_STATE_IDLE    0x0100
#define SLIME_STATE_MOVE    0x0200
#define SLIME_SPEED         0x0180

// -- constants --
// const u8 LOOKS[4] = {0, 1, 0, 2}; //this is making compilation b0rked. need to remedy it somehow.

// const s8 SLIME_IDLE[4][2] =
// {
//     {}
// }

// const s8 

// -- prototypes --
// static void slime_anim_idle(Sprite *slime);
// static void slime_anim_move(Sprite *slime);

#endif