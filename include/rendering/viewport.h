// ========== 極性 -Kyokusei- ============================ viewport.h ========================== //
#ifndef __RENDER_VIEWPORT__
#define __RENDER_VIEWPORT__

#include <tonc.h>
#include <tonc_memmap.h>

// #include  // (i forgot what file was going to be included here.)

typedef struct Viewport
{
    int x, xmin, xmax, xpage;
    int y, ymin, ymax, ypage;
} Viewport;

// -- prototypes --
void vp_set_position(Viewport *view, FIXED x, FIXED y);

// -- inline functions --
INLINE void vp_center(Viewport *view, FIXED x, FIXED y)
{ vp_set_position(view, x - view->xpage/2, y - view->ypage/2); }

#endif
