// ========== 極性 -Kyokusei- ============================ textbox.h =========================== //
#ifndef __RENDER_TEXTBOX__
#define __RENDER_TEXTBOX__

#include <tonc.h>
#include <tonc_tte.h>

// -- prototypes --
void init_textbox(int bgnr, int left, int top, int right, int bottom);

#endif