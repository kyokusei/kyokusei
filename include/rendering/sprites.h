// ========== 極性 -Kyokusei- ============================ sprites.h ============================= //
#ifndef __RENDER_SPRITES__
#define __RENDER_SPRITES__

#include <tonc_types.h>

// #include "../../gfx/emy_slime.h"
#include "gfx_master.h"

#define SPRITE_IDLE     0x0100
#define SPRITE_MOVE     0x0200

enum LookDirection
{
    RIGHT = 0,
    DOWN,
    LEFT,
    UP
};

typedef struct Sprite
{
    FIXED   pos_x, pos_y;
    FIXED   vel_x, vel_y;
    u16     state;
    u8      direction;
    u8      obj_index;
    FIXED   frame;
} Sprite;


// == prototypes ==
void sprite_init(Sprite *sprite, int x, int y, int obj_id);
void sprite_set_state(Sprite *sprite, u32 state);
void sprite_input(Sprite *sprite);

void sprite_move(Sprite *sprite);
void sprite_animate(Sprite *sprite);

#endif