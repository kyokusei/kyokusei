// ========== 極性 -Kyokusei- ============================ main.cpp ============================ //
#ifdef __cplusplus
extern "C" {
#endif

// library includes
#include <stdio.h>
#include <string.h>
#include <tonc.h>
#include <tonc_tte.h>

// resource includes
#include "fonts.h"
#include "soundbank.h"
#include "soundbank_bin.h"

// logic includes
#include "mechanics.h"
#include "rendering.h"
#include "logic.h"

// #include "bitmap_etc.h"

OBJ_ATTR obj_buffer[128];
OBJ_AFFINE *obj_affine_buffer = (OBJ_AFFINE*)obj_buffer;

// main game loop
int main()
{
    // int ii, jj;
    irq_init(NULL);
    irq_add(II_VBLANK, NULL);


    REG_DISPCNT = DCNT_MODE0 | DCNT_BG0;

    tte_init_se_default(0, BG_CBB(0)|BG_CBB(31));

    tte_write("#{P:82,64");
    // tte_write("REEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEe");
    tte_write("I have no idea how i'm gonna get this done in time.");

    while(1);

    return 0;
}

#ifdef __cplusplus
};
#endif