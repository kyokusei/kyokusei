// ========== 極性 -Kyokusei- ============================ map_info.h ========================== //

#include <string.h>

#include "rendering/map_info.h"

void bg_tile_init(MapInfo *bg_tile, int bgnr, u32 ctrl, const void *map, u32 map_width, u32 map_height)
{
    memset(bg_tile, 0, sizeof(MapInfo));

    bg_tile->flags = bgnr;
    bg_tile->count = ctrl;
    bg_tile->dstMap = se_mem[BFN_GET(ctrl, BG_SBB)];

    REG_BGCNT[bgnr] = ctrl;
    REG_BG_OFS[bgnr].x = 0;
    REG_BG_OFS[bgnr].x = 0;

    bg_tile->srcMap = (SCR_ENTRY*)map;
    bg_tile->srcMapWidth = map_width;
    bg_tile->srcMapHeight = map_height;

    // int ix;
    // int iy;
    SCR_ENTRY *dest = bg_tile->dstMap;
    SCR_ENTRY *src  = bg_tile->srcMap;
    for (int iy = 0; iy < 32; iy++) {
        for (int ix = 0; ix < 32; ix++) {
            dest[iy*32+ix] = src[iy*bg_tile->srcMapWidth+ix];
        }// end for ix
    }// end for iy
}// end bg_tile_init()

void bg_tile_column_copy(MapInfo *bg_tile, int tx, int ty)
{
    int y0 = ty & 31;

    int sourceP = bg_tile->srcMapWidth;
    SCR_ENTRY *sourceL = &bg_tile->srcMap[ty*sourceP+tx];
    SCR_ENTRY *destL = &bg_tile->dstMap[y0*32+(tx&31)];

    for (int iy = 0; iy < 32; iy++) {
        *destL = *sourceL;
        destL += 32;
        sourceL += sourceP;
    }

    destL -= 1024;

    for (int iy = 0; iy < y0; iy++) {
        *destL = *sourceL;
        destL += 32;
        sourceL += sourceP;
    }
}// end bg_tile_column_copy()

void bg_tile_row_copy(MapInfo *bg_tile, int tx, int ty)
{
    int x0 = tx & 31;

    int sourceP = bg_tile->srcMapWidth;
    SCR_ENTRY *sourceL = &bg_tile->srcMap[ty*sourceP+tx];
    SCR_ENTRY *destL = &bg_tile->dstMap[(ty&31)*32+x0];

    for (int ix = 0; ix < 32; ix++) {
        *destL++= *sourceL;
        // destL += 32;
        // sourceL += sourceP;
    }

    destL -= 32;

    for (int ix = 0; ix < x0; ix++) {
        *destL++ = *sourceL;
        // destL += 32;
        // sourceL += sourceP;
    }
}// end bg_tile_row_copy()

void bg_tile_update(MapInfo *bg_tile, Viewport *view)
{
    // pixel coordinates
    int vx = view->x;
    int vy = view->y;
    int bx = bg_tile->mapX;
    int by = bg_tile->mapY;

    // tile coordinates
    int t_vx = vx>>3;
    int t_vy = vy>>3;
    int t_bx = bx>>3;
    int t_by = by>>3;

    // need to get another row/col when viewport shows another row/col.
    // updates background if/when tile coordinates have changed.
    if (t_vx < t_bx) { // left change
        bg_tile_column_copy(bg_tile, t_vx, t_vy);
    } else if (t_vx > t_bx) { // right change
        bg_tile_column_copy(bg_tile, t_vx+31, t_vy);
    }

    if (t_vy < t_by) { // top change
        bg_tile_row_copy(bg_tile, t_vx, t_vy);
    } else if (t_vy > t_by) { // bottom change
        bg_tile_row_copy(bg_tile, t_vx, t_vy+31);
    }

    int bgnr = bg_tile->flags;
    REG_BG_OFS[bgnr].x = bg_tile->mapX = vx;
    REG_BG_OFS[bgnr].y = bg_tile->mapY = vy;
}// end bg_tile_update()