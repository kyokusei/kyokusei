// ========== 極性 -Kyokusei- ============================ viewport.c ========================== //

#include "rendering/viewport.h"

// INLINE void vp_center(Viewport *view, FIXED x, FIXED y)
// { vp_set_position(view, x - view->xpage/2, y - view->ypage/2); }

void vp_set_position(Viewport *view, FIXED x, FIXED y)
{
    view->x = clamp(x, view->xmin, view->xmax - view->xpage);
    view->y = clamp(x, view->ymin, view->ymax - view->ypage);
}