// ========== 極性 -Kyokusei- ============================ textbox.h =========================== //

#include "rendering/textbox.h"

void init_textbox(int bgnr, int left, int top, int right, int bottom)
{
    tte_set_margins(left, top, right, bottom);
    REG_DISPCNT |= DCNT_WIN0;

    REG_WIN0H = left<<8 | right;
    REG_WIN0V = top<<8 | bottom;
    REG_WIN0CNT = WIN_ALL | WIN_BLD;
    REG_WINOUTCNT = WIN_ALL;

    REG_BLDCNT = (BLD_ALL&~BIT(bgnr)) | BLD_BLACK;
    REG_BLDY = 5;
}