// ========== 極性 -Kyokusei- ============================ sprites.c ========================== //

#include "rendering/sprites.h"
#include "rendering/viewport.h"

void sprite_init(Sprite *sprite, int x, int y, int obj_id)
{

}

void sprite_set_state(Sprite *sprite, u32 state)
{
    sprite->state = state;
    sprite->frame = 0;
}

void sprite_input(Sprite *sprite)
{

}

void sprite_move(Sprite *sprite)
{

}

void sprite_animate(Sprite *sprite)
{

}