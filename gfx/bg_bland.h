
//{{BLOCK(bg_bland)

//======================================================================
//
//	bg_bland, 240x160@4, 
//	+ palette 11 entries, not compressed
//	+ 72 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 30x20 
//	Total size: 22 + 2304 + 1200 = 3526
//
//	Time-stamp: 2019-10-20, 03:03:17
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_BG_BLAND_H
#define GRIT_BG_BLAND_H

#define bg_blandTilesLen 2304
extern const unsigned int bg_blandTiles[576];

#define bg_blandMapLen 1200
extern const unsigned short bg_blandMap[600];

#define bg_blandPalLen 22
extern const unsigned short bg_blandPal[12];

#endif // GRIT_BG_BLAND_H

//}}BLOCK(bg_bland)
