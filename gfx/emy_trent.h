
//{{BLOCK(emy_trent)

//======================================================================
//
//	emy_trent, 16x32@4, 
//	+ palette 11 entries, not compressed
//	+ 9 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 2x2 
//	Metatiled by 1x2
//	Total size: 22 + 288 + 20 + 8 = 338
//
//	Time-stamp: 2019-10-20, 01:25:19
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_EMY_TRENT_H
#define GRIT_EMY_TRENT_H

#define emy_trentTilesLen 288
extern const unsigned int emy_trentTiles[72];

#define emy_trentMetaTilesLen 20
extern const unsigned short emy_trentMetaTiles[10];

#define emy_trentMetaMapLen 8
extern const unsigned short emy_trentMetaMap[4];

#define emy_trentPalLen 22
extern const unsigned short emy_trentPal[12];

#endif // GRIT_EMY_TRENT_H

//}}BLOCK(emy_trent)
