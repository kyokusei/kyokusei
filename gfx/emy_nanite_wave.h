
//{{BLOCK(emy_nanite_wave)

//======================================================================
//
//	emy_nanite_wave, 64x64@4, 
//	+ palette 12 entries, not compressed
//	+ 30 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 2x2 
//	Metatiled by 4x4
//	Total size: 24 + 960 + 160 + 8 = 1152
//
//	Time-stamp: 2019-10-20, 01:25:22
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_EMY_NANITE_WAVE_H
#define GRIT_EMY_NANITE_WAVE_H

#define emy_nanite_waveTilesLen 960
extern const unsigned int emy_nanite_waveTiles[240];

#define emy_nanite_waveMetaTilesLen 160
extern const unsigned short emy_nanite_waveMetaTiles[80];

#define emy_nanite_waveMetaMapLen 8
extern const unsigned short emy_nanite_waveMetaMap[4];

#define emy_nanite_wavePalLen 24
extern const unsigned short emy_nanite_wavePal[12];

#endif // GRIT_EMY_NANITE_WAVE_H

//}}BLOCK(emy_nanite_wave)
