
//{{BLOCK(emy_sentry)

//======================================================================
//
//	emy_sentry, 16x16@4, 
//	+ palette 11 entries, not compressed
//	+ 5 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 1x1 
//	Metatiled by 2x2
//	Total size: 22 + 160 + 16 + 2 = 200
//
//	Time-stamp: 2019-10-20, 01:25:28
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_EMY_SENTRY_H
#define GRIT_EMY_SENTRY_H

#define emy_sentryTilesLen 160
extern const unsigned int emy_sentryTiles[40];

#define emy_sentryMetaTilesLen 16
extern const unsigned short emy_sentryMetaTiles[8];

#define emy_sentryMetaMapLen 2
extern const unsigned short emy_sentryMetaMap[2];

#define emy_sentryPalLen 22
extern const unsigned short emy_sentryPal[12];

#endif // GRIT_EMY_SENTRY_H

//}}BLOCK(emy_sentry)
