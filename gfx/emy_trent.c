
//{{BLOCK(emy_trent)

//======================================================================
//
//	emy_trent, 16x32@4, 
//	+ palette 11 entries, not compressed
//	+ 9 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 2x2 
//	Metatiled by 1x2
//	Total size: 22 + 288 + 20 + 8 = 338
//
//	Time-stamp: 2019-10-20, 01:25:19
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

const unsigned int emy_trentTiles[72] __attribute__((aligned(4))) __attribute__((visibility("hidden")))=
{
	0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,
	0x00000000,0x00000000,0x07300000,0x73800000,0x38400000,0x81400000,0x84800000,0x88300500,
	0x88000520,0x38005220,0x38375260,0x88375260,0x88352660,0x13752660,0x83752660,0x88875260,
	0x00000000,0x00000000,0x00000377,0x00000433,0x00000418,0x00000841,0x00000384,0x00000738,
	0x00000073,0x00000077,0x00773337,0x00733888,0x07338884,0x07380841,0x07380784,0x07830788,
	0x88375260,0x88005220,0x38000520,0x38000500,0x73800000,0x73800000,0x73880000,0x73880000,
	0x77380000,0x07388000,0x07738000,0x07333000,0x03303000,0x03303300,0x73003300,0x00000000,
	0x07807388,0x07307338,0x07000738,0x03000738,0x00000738,0x00007888,0x00007887,0x00078380,

	0x00783880,0x00783800,0x00783800,0x07883800,0x07803000,0x08038000,0x00038000,0x00000000,
};

const unsigned short emy_trentMetaTiles[10] __attribute__((aligned(4))) __attribute__((visibility("hidden")))=
{
	0x0000,0x0000,0x0001,0x0002,0x0003,0x0004,0x0005,0x0006,
	0x0007,0x0008,
};

const unsigned short emy_trentMetaMap[4] __attribute__((aligned(4))) __attribute__((visibility("hidden")))=
{
	0x0001,0x0002,0x0003,0x0004,
};

const unsigned short emy_trentPal[12] __attribute__((aligned(4))) __attribute__((visibility("hidden")))=
{
	0x0000,0x3C42,0x00B1,0x0444,0x2821,0x0049,0x1535,0x0022,
	0x0866,0x0000,0x0000,
};

//}}BLOCK(emy_trent)
