
//{{BLOCK(echo)

//======================================================================
//
//	echo, 16x32@4, 
//	+ palette 8 entries, not compressed
//	+ 9 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 2x2 
//	Metatiled by 1x2
//	Total size: 16 + 288 + 20 + 8 = 332
//
//	Time-stamp: 2019-10-20, 01:25:09
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

const unsigned int echoTiles[72] __attribute__((aligned(4))) __attribute__((visibility("hidden")))=
{
	0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,
	0x00000000,0x00000000,0x00000000,0x00000000,0x30000000,0x33200000,0x23601000,0x52510000,
	0x15100000,0x22400000,0x42000000,0x00000000,0x23000000,0x22330000,0x54223000,0x11102000,
	0x00000000,0x00000000,0x00000400,0x00000420,0x00004623,0x00001623,0x00004152,0x00004215,
	0x00004621,0x00000462,0x00000004,0x00000000,0x00000042,0x00004514,0x00006415,0x11062241,
	0x35501000,0x33402000,0x33305000,0x22301000,0x22202000,0x66200000,0x44400000,0x22600000,
	0x02230000,0x00250000,0x00610000,0x00620000,0x00420000,0x00061000,0x00042500,0x00000000,
	0x00063023,0x05063022,0x00122022,0x00511022,0x00052062,0x00000446,0x00000224,0x00000622,

	0x00000623,0x00000650,0x00004210,0x00006200,0x00042300,0x00061000,0x00042500,0x00000000,
};

const unsigned short echoMetaTiles[10] __attribute__((aligned(4))) __attribute__((visibility("hidden")))=
{
	0x0000,0x0000,0x0001,0x0002,0x0003,0x0004,0x0005,0x0006,
	0x0007,0x0008,
};

const unsigned short echoMetaMap[4] __attribute__((aligned(4))) __attribute__((visibility("hidden")))=
{
	0x0001,0x0002,0x0003,0x0004,
};

const unsigned short echoPal[8] __attribute__((aligned(4))) __attribute__((visibility("hidden")))=
{
	0x0000,0x017F,0x1084,0x294A,0x0421,0x19FF,0x0842,0x0000,
};

//}}BLOCK(echo)
