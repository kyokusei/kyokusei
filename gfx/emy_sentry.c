
//{{BLOCK(emy_sentry)

//======================================================================
//
//	emy_sentry, 16x16@4, 
//	+ palette 11 entries, not compressed
//	+ 5 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 1x1 
//	Metatiled by 2x2
//	Total size: 22 + 160 + 16 + 2 = 200
//
//	Time-stamp: 2019-10-20, 01:25:28
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

const unsigned int emy_sentryTiles[40] __attribute__((aligned(4))) __attribute__((visibility("hidden")))=
{
	0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,
	0x00000000,0x77733000,0x66888000,0x14633000,0x14600000,0x99230000,0x22387000,0x83877000,
	0x00000000,0x00033777,0x00038888,0x00073331,0x00000082,0x00000882,0x00073338,0x00077888,
	0x78330000,0x07770000,0x30050000,0x00055500,0x00300500,0x00000500,0x00000500,0x00000000,
	0x00000333,0x00000777,0x00000500,0x00555500,0x00500030,0x00500000,0x00500000,0x00000000,
};

const unsigned short emy_sentryMetaTiles[8] __attribute__((aligned(4))) __attribute__((visibility("hidden")))=
{
	0x0000,0x0000,0x0000,0x0000,0x0001,0x0002,0x0003,0x0004,
};

const unsigned short emy_sentryMetaMap[2] __attribute__((aligned(4))) __attribute__((visibility("hidden")))=
{
	0x0001,
};

const unsigned short emy_sentryPal[12] __attribute__((aligned(4))) __attribute__((visibility("hidden")))=
{
	0x0000,0x7FFF,0x000C,0x0421,0x001D,0x14A5,0x0013,0x0C63,
	0x0842,0x001F,0x0000,
};

//}}BLOCK(emy_sentry)
