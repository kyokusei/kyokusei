
//{{BLOCK(emy_slime)

//======================================================================
//
//	emy_slime, 96x32@4, 
//	+ palette 4 entries, not compressed
//	+ 38 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 6x2 
//	Metatiled by 2x2
//	Total size: 8 + 1216 + 96 + 24 = 1344
//
//	Time-stamp: 2019-10-20, 01:25:33
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_EMY_SLIME_H
#define GRIT_EMY_SLIME_H

#define emy_slimeTilesLen 1216
extern const unsigned int emy_slimeTiles[304];

#define emy_slimeMetaTilesLen 96
extern const unsigned short emy_slimeMetaTiles[48];

#define emy_slimeMetaMapLen 24
extern const unsigned short emy_slimeMetaMap[12];

#define emy_slimePalLen 8
extern const unsigned short emy_slimePal[4];

#endif // GRIT_EMY_SLIME_H

//}}BLOCK(emy_slime)
