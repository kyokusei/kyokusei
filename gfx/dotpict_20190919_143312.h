
//{{BLOCK(dotpict_20190919_143312)

//======================================================================
//
//	dotpict_20190919_143312, 16x16@4, 
//	+ palette 16 entries, not compressed
//	+ 4 tiles not compressed
//	Total size: 32 + 128 = 160
//
//	Time-stamp: 2019-09-19, 19:01:10
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_DOTPICT_20190919_143312_H
#define GRIT_DOTPICT_20190919_143312_H

#define dotpict_20190919_143312TilesLen 128
extern const unsigned int dotpict_20190919_143312Tiles[32];

#define dotpict_20190919_143312PalLen 32
extern const unsigned short dotpict_20190919_143312Pal[16];

#endif // GRIT_DOTPICT_20190919_143312_H

//}}BLOCK(dotpict_20190919_143312)
