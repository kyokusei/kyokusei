
//{{BLOCK(echo)

//======================================================================
//
//	echo, 16x32@4, 
//	+ palette 8 entries, not compressed
//	+ 9 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 2x2 
//	Metatiled by 1x2
//	Total size: 16 + 288 + 20 + 8 = 332
//
//	Time-stamp: 2019-10-20, 01:25:09
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_ECHO_H
#define GRIT_ECHO_H

#define echoTilesLen 288
extern const unsigned int echoTiles[72];

#define echoMetaTilesLen 20
extern const unsigned short echoMetaTiles[10];

#define echoMetaMapLen 8
extern const unsigned short echoMetaMap[4];

#define echoPalLen 16
extern const unsigned short echoPal[8];

#endif // GRIT_ECHO_H

//}}BLOCK(echo)
