
//{{BLOCK(emy_chroncha)

//======================================================================
//
//	emy_chroncha, 32x32@4, 
//	+ palette 9 entries, not compressed
//	+ 11 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 2x2 
//	Metatiled by 2x2
//	Total size: 18 + 352 + 40 + 8 = 418
//
//	Time-stamp: 2019-10-20, 01:25:25
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_EMY_CHRONCHA_H
#define GRIT_EMY_CHRONCHA_H

#define emy_chronchaTilesLen 352
extern const unsigned int emy_chronchaTiles[88];

#define emy_chronchaMetaTilesLen 40
extern const unsigned short emy_chronchaMetaTiles[20];

#define emy_chronchaMetaMapLen 8
extern const unsigned short emy_chronchaMetaMap[4];

#define emy_chronchaPalLen 18
extern const unsigned short emy_chronchaPal[10];

#endif // GRIT_EMY_CHRONCHA_H

//}}BLOCK(emy_chroncha)
