
@{{BLOCK(dotpict_20190919_143312)

@=======================================================================
@
@	dotpict_20190919_143312, 16x16@4, 
@	+ palette 16 entries, not compressed
@	+ 4 tiles not compressed
@	Total size: 32 + 128 = 160
@
@	Time-stamp: 2019-09-19, 19:01:10
@	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global dotpict_20190919_143312Tiles		@ 128 unsigned chars
	.hidden dotpict_20190919_143312Tiles
dotpict_20190919_143312Tiles:
	.word 0x4F7C2B60,0x4F7C2B60,0x4F7C2B60,0x4F7C2B60,0x4F7C2B60,0x4F7C2B60,0x4F7C2B60,0x4F7C2B60
	.word 0x4F7C2B60,0x4F7C2B60,0x4F7C2B60,0x4F7C2B60,0x4F7C2B60,0x4F7C2B60,0x4F7C2B60,0x4F7C2B60
	.word 0x93A5D1E8,0x93A5D1E8,0x93A5D1E8,0x93A5D1E8,0x93A5D1E8,0x93A5D1E8,0x93A5D1E8,0x93A5D1E8
	.word 0x93A5D1E8,0x93A5D1E8,0x93A5D1E8,0x93A5D1E8,0x93A5D1E8,0x93A5D1E8,0x93A5D1E8,0x93A5D1E8

	.section .rodata// __________ generated: 
	.align	2// __________ generated: 
	.global dotpict_20190919_143312Pal		@ 32 unsigned chars
	.hidden dotpict_20190919_143312Pal
dotpict_20190919_143312Pal:
	.hword 0x1868,0x269F,0x20B8,0x67FF,0x197F,0x2B7F,0x20AF,0x1CFE
	.hword 0x1DDF,0x7FFF,0x3FDF,0x20B4,0x20DC,0x26FF,0x223F,0x193F

@}}BLOCK(dotpict_20190919_143312)
