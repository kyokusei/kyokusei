# Summary of Game

[Kyokusei Guide](./index.md)  
[Preface](./preface.md)  
[Controls](./controls.md)  
[Intro](./intro.md)

## Game Plot
- [Area 1](./ch1/00.md)
    - [item 1](./ch1/01.md)
    
- [Area 2](./ch2/00.md)
    - [item 1](./ch2/01.md)
    
- [Area 3](./ch3/00.md)
    - [item 1](./ch3/01.md)
    
- [Area 4](./ch4/00.md)
    - [item 1](./ch4/01.md)
    
- [Area 5](./ch5/00.md)
    - [item 1](./ch5/01.md)
    
- [Appendices](./apdx/00.md)
    - [Appendix 1](./apdx/01.md)
    - [Appendix 2](./apdx/02.md)